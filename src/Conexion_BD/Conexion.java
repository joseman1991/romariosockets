package Conexion_BD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.text.BadLocationException;
import sistemabancario.rpc.rmi_MAIN.Main_Principal;

/**
 *
 * @author Ivan Luis Jimenez
 */
public class Conexion {

    Connection con = null;

    public Connection conex() throws BadLocationException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3307/bankito?autoReconnect=true&useSSL=false", "root", "mariadb");
            Main_Principal.Write("[Servidor BD] Conexion establecida");
        } catch (ClassNotFoundException | SQLException | BadLocationException e) {
            e.printStackTrace();
            Main_Principal.Write("[Servidor BD] NO se pudo iniciar la Base de Datos");
        }
        return con;
    }
}
