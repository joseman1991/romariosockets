/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemabancario.rpc.rmi_MAIN;

 
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author JOSE
 */
public class Hilo implements Runnable {

    
    private ServerSocket socketServer;
   

    private ObjectOutputStream outO;
    private ObjectInputStream inO;

    public Hilo(ServerSocket socket) {
        this.socketServer = socket;     
    }

    @Override
    public void run() {
        System.out.println("escuchando puerto " + socketServer.getLocalPort());
        while (true) {
            try {
                Socket socket = socketServer.accept();
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                DataInputStream in;
                in = new DataInputStream(socket.getInputStream());
                String c = in.readUTF();
                switch (c) {

                    case "d":
                        System.out.println(String.format("Dato recibido de %s desde puerto %d", socket.getInetAddress().getHostName(), socket.getLocalPort()));
                        inO = new ObjectInputStream(socket.getInputStream());
                  
                        out.writeUTF("");
                        break;

                    case "lg":
                        inO = new ObjectInputStream(socket.getInputStream());
                      
                        
                        outO = new ObjectOutputStream(socket.getOutputStream());
                        
                        break;

                    case "l":
                       
                        outO = new ObjectOutputStream(socket.getOutputStream());
                       
                        break;

                    case "i":
                        inO = new ObjectInputStream(socket.getInputStream());
                        
                    
                         
                        break;

                    case "u":
                        outO = new ObjectOutputStream(socket.getOutputStream());
                      
                        
                        break;

                    case "t":
                        int puerto = in.readInt();
                        boolean estado = test(puerto);
                        out.writeBoolean(estado);
                        break;

                    case "g":
                        inO = new ObjectInputStream(socket.getInputStream());
                        
                     
                        
                        break;

                    case "ok":
                        out.writeBoolean(true);
                        break;

                    case "lok":
                        System.out.println("Usuario logueado desde puerto: " + socketServer.getLocalPort());
                        break;

                    default:
                        System.out.println("enviaste " + c);
                        break;
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    

    private boolean test(int puerto) {
        try {
            ServerSocket s = new ServerSocket(puerto);
            s.close();
            return true;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            System.out.println("Puerto no disponible");
            return false;
        }
    }

   

  

    

}
